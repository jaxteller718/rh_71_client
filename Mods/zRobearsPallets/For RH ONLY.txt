Only thing I couldn't figure out to Xpath was to remove the bundles that get replaced, the only way I know how is to comment them out, in Items.xml, Reciepe.xml.
The palletofGas has an issue, which I can't figure out in the time I've spent on it.  It will let you pack the palletOfGas if you've read the book, but unless you have yeah science 2 it won't let you unpack it and I can't figure out how to change that.
All the pallets work in vanilla packing and unpacking.
All this here is for suggestions RH resources, up to you man!
palletOfRawIron,blocks,Blocks,New,Pallet of Raw Iron,,,,,
palletOfRawLead,blocks,Blocks,New,Pallet of Raw Lead,,,,,
palletOfRawBrass,blocks,Blocks,New,Pallet of Raw Brass,,,,,
palletOfAnimalHide,blocks,Blocks,New,Pallet of Animal Hides,,,,,
palletOfTwine,blocks,Blocks,New,Pallet of Twine,,,,,
palletOfCharcoal,blocks,Blocks,New,Pallet of Charcoal,,,,,
palletOfSticks,blocks,Blocks,New,Pallet of Sticks,,,,,
<!-- Suggested RH Resources Pallets -->
<block name="palletOfRawIron">
	<!-- brown boxes, full -->
	<property name="Material" value="MCardboardLoot"/>
	<property name="CustomIcon" value="palletBrownBoxesBase"/>
	<property name="CanPickup" value="true"/>
	<property name="Stacknumber" value="1"/>
	<property name="MaxDamage" value="300"/>
	<property name="UnlockedBy" value="perkArtOfMiningPallets"/>
	<property name="Shape" value="ModelEntity"/>
	<property name="Model" value="Entities/Industrial/pallet_boxes_basePrefab"/>
	<property name="ImposterExchange" value="imposterBlock" param1="149"/>
	<property name="Collide" value="sight,movement,melee,bullet,arrow,rocket"/>
	<property name="Path" value="solid"/>
	<drop event="Destroy" name="palletOfRawIron" count="1"/>
	<property name="DescriptionKey" value="decorationBlockGroupDesc"/>
	<property name="FilterTags" value="fdecor,fother"/>
	<property class="RepairItems">
		<property name="resourceIronFragment" value="10"/>
	</property>
	<property name="Group" value="Resources"/>
	<property name="SellableToTrader" value="false"/>
</block>
<block name="palletOfRawLead">
	<!-- brown boxes, full -->
	<property name="Material" value="MCardboardLoot"/>
	<property name="CustomIcon" value="palletBrownBoxesBase"/>
	<property name="CanPickup" value="true"/>
	<property name="Stacknumber" value="1"/>
	<property name="MaxDamage" value="300"/>
	<property name="UnlockedBy" value="perkArtOfMiningPallets"/>
	<property name="Shape" value="ModelEntity"/>
	<property name="Model" value="Entities/Industrial/pallet_boxes_basePrefab"/>
	<property name="ImposterExchange" value="imposterBlock" param1="149"/>
	<property name="Collide" value="sight,movement,melee,bullet,arrow,rocket"/>
	<property name="Path" value="solid"/>
	<drop event="Destroy" name="palletOfRawLead" count="1"/>
	<property name="DescriptionKey" value="decorationBlockGroupDesc"/>
	<property name="FilterTags" value="fdecor,fother"/>
	<property class="RepairItems">
		<property name="resourceLeadFragmentRH" value="10"/>
	</property>
	<property name="Group" value="Resources"/>
	<property name="SellableToTrader" value="false"/>
</block>
<block name="palletOfRawBrass">
	<!-- brown boxes, full -->
	<property name="Material" value="MCardboardLoot"/>
	<property name="CustomIcon" value="palletBrownBoxesBase"/>
	<property name="CanPickup" value="true"/>
	<property name="Stacknumber" value="1"/>
	<property name="MaxDamage" value="300"/>
	<property name="UnlockedBy" value="perkArtOfMiningPallets"/>
	<property name="Shape" value="ModelEntity"/>
	<property name="Model" value="Entities/Industrial/pallet_boxes_basePrefab"/>
	<property name="ImposterExchange" value="imposterBlock" param1="149"/>
	<property name="Collide" value="sight,movement,melee,bullet,arrow,rocket"/>
	<property name="Path" value="solid"/>
	<drop event="Destroy" name="palletOfRawBrass" count="1"/>
	<property name="DescriptionKey" value="decorationBlockGroupDesc"/>
	<property name="FilterTags" value="fdecor,fother"/>
	<property class="RepairItems">
		<property name="resourceBrassFragmentRH" value="10"/>
	</property>
	<property name="Group" value="Resources"/>
	<property name="SellableToTrader" value="false"/>
</block>
<block name="palletOfAnimalHide">
	<!-- brown boxes, full -->
	<property name="Material" value="MCardboardLoot"/>
	<property name="CustomIcon" value="palletBrownBoxesBase"/>
	<property name="CanPickup" value="true"/>
	<property name="Stacknumber" value="1"/>
	<property name="MaxDamage" value="300"/>
	<property name="UnlockedBy" value="perkArtOfMiningPallets"/>
	<property name="Shape" value="ModelEntity"/>
	<property name="Model" value="Entities/Industrial/pallet_boxes_basePrefab"/>
	<property name="ImposterExchange" value="imposterBlock" param1="149"/>
	<property name="Collide" value="sight,movement,melee,bullet,arrow,rocket"/>
	<property name="Path" value="solid"/>
	<drop event="Destroy" name="palletOfAnimalHide" count="1"/>
	<property name="DescriptionKey" value="decorationBlockGroupDesc"/>
	<property name="FilterTags" value="fdecor,fother"/>
	<property class="RepairItems">
		<property name="resourceAnimalHideRH" value="10"/>
	</property>
	<property name="Group" value="Resources"/>
	<property name="SellableToTrader" value="false"/>
</block>
<block name="palletOfTwine">
	<!-- brown boxes, full -->
	<property name="Material" value="MCardboardLoot"/>
	<property name="CustomIcon" value="palletBrownBoxesBase"/>
	<property name="CanPickup" value="true"/>
	<property name="Stacknumber" value="1"/>
	<property name="MaxDamage" value="300"/>
	<property name="UnlockedBy" value="perkArtOfMiningPallets"/>
	<property name="Shape" value="ModelEntity"/>
	<property name="Model" value="Entities/Industrial/pallet_boxes_basePrefab"/>
	<property name="ImposterExchange" value="imposterBlock" param1="149"/>
	<property name="Collide" value="sight,movement,melee,bullet,arrow,rocket"/>
	<property name="Path" value="solid"/>
	<drop event="Destroy" name="palletOfTwine" count="1"/>
	<property name="DescriptionKey" value="decorationBlockGroupDesc"/>
	<property name="FilterTags" value="fdecor,fother"/>
	<property class="RepairItems">
		<property name="resourceTwineRH" value="10"/>
	</property>
	<property name="Group" value="Resources"/>
	<property name="SellableToTrader" value="false"/>
</block>
<block name="palletOfCharcoal">
	<!-- brown boxes, full -->
	<property name="Material" value="MCardboardLoot"/>
	<property name="CustomIcon" value="palletBrownBoxesBase"/>
	<property name="CanPickup" value="true"/>
	<property name="Stacknumber" value="1"/>
	<property name="MaxDamage" value="300"/>
	<property name="UnlockedBy" value="perkArtOfMiningPallets"/>
	<property name="Shape" value="ModelEntity"/>
	<property name="Model" value="Entities/Industrial/pallet_boxes_basePrefab"/>
	<property name="ImposterExchange" value="imposterBlock" param1="149"/>
	<property name="Collide" value="sight,movement,melee,bullet,arrow,rocket"/>
	<property name="Path" value="solid"/>
	<drop event="Destroy" name="palletOfCharcoal" count="1"/>
	<property name="DescriptionKey" value="decorationBlockGroupDesc"/>
	<property name="FilterTags" value="fdecor,fother"/>
	<property class="RepairItems">
		<property name="resourceCharcoalRH" value="10"/>
	</property>
	<property name="Group" value="Resources"/>
	<property name="SellableToTrader" value="false"/>
</block>
<block name="palletOfSticks">
	<!-- brown boxes, full -->
	<property name="Material" value="MCardboardLoot"/>
	<property name="CustomIcon" value="palletBrownBoxesBase"/>
	<property name="CanPickup" value="true"/>
	<property name="Stacknumber" value="1"/>
	<property name="MaxDamage" value="300"/>
	<property name="UnlockedBy" value="perkArtOfMiningPallets"/>
	<property name="Shape" value="ModelEntity"/>
	<property name="Model" value="Entities/Industrial/pallet_boxes_basePrefab"/>
	<property name="ImposterExchange" value="imposterBlock" param1="149"/>
	<property name="Collide" value="sight,movement,melee,bullet,arrow,rocket"/>
	<property name="Path" value="solid"/>
	<drop event="Destroy" name="palletOfSticks" count="1"/>
	<property name="DescriptionKey" value="decorationBlockGroupDesc"/>
	<property name="FilterTags" value="fdecor,fother"/>
	<property class="RepairItems">
		<property name="resourceStickRH" value="10"/>
	</property>
	<property name="Group" value="Resources"/>
	<property name="SellableToTrader" value="false"/>
</block>
<--! Goes in Pallet Book in Progression -->
	palletOfRawIron,palletOfRawLead,palletOfRawBrass,palletOfAnimalHide,palletOfTwine,palletOfCharcoal,palletOfSticks
	resourceIronFragment,resourceLeadFragmentRH,resourceBrassFragmentRH,resourceAnimalHideRH,resourceTwineRH,resourceCharcoalRH,resourceStickRH
	<!-- RH Resource Pallets -->
	<recipe name="palletOfRawIron" count="1" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="resourceIronFragment" count="2000"/>
		<ingredient name="palletEmpty" count="1"/>
	</recipe>
	<recipe name="palletOfRawLead" count="1" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="resourceLeadFragmentRH" count="2000"/>
		<ingredient name="palletEmpty" count="1"/>
	</recipe>
	<recipe name="palletOfRawBrass" count="1" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="resourceBrassFragmentRH" count="2000"/>
		<ingredient name="palletEmpty" count="1"/>
	</recipe>
	<recipe name="palletOfAnimalHide" count="1" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="resourceAnimalHideRH" count="2000"/>
		<ingredient name="palletEmpty" count="1"/>
	</recipe>
	<recipe name="palletOfTwine" count="1" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="resourceTwineRH" count="2000"/>
		<ingredient name="palletEmpty" count="1"/>
	</recipe>
	<recipe name="palletOfCharcoal" count="1" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="resourceCharcoalRH" count="2000"/>
		<ingredient name="palletEmpty" count="1"/>
	</recipe>
	<recipe name="palletOfSticks" count="1" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="resourceStickRH" count="2000"/>
		<ingredient name="palletEmpty" count="1"/>
	</recipe>
	<!-- Unpacking Pallets -->
	<recipe name="resourceIronFragment" count="2000" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="palletOfRawIron" count="1"/>
	</recipe>
	<recipe name="resourceLeadFragmentRH" count="2000" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="palletOfRawLead" count="1"/>
	</recipe>
	<recipe name="resourceBrassFragmentRH" count="2000" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="palletOfAnimalHide" count="1"/>
	</recipe>
	<recipe name="resourceAnimalHideRH" count="2000" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="resourceAnimalHideRH" count="1"/>
	</recipe>
	<recipe name="resourceTwineRH" count="2000" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="palletOfTwine" count="1"/>
	</recipe>
	<recipe name="resourceCharcoalRH" count="2000" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="palletOfCharcoal" count="1"/>
	</recipe>
	<recipe name="resourceStickRH" count="2000" craft_area="workbench" craft_time="15" tags="learnable">
		<ingredient name="palletOfSticks" count="1"/>
	</recipe>
	